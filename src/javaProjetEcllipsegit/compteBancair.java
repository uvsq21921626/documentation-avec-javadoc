package javaProjetEcllipsegit;

/**
 * classe de compte bancaire qui est définit par un seul attribut solde avec un
 * ensemble de méthodes
 * 
 * @author Lynda Hadjab
 */

public class compteBancair {
	public int solde;

	compteBancair(int sol) {
		this.solde = sol;
	}

	compteBancair() {

	}

	public void consultationS() {

		System.out.println("le solde est " + this.solde);
	}

	public void virement(compteBancair c1, compteBancair c2, int somme) throws Exceptionvirement {
		// try {
		if (c1.solde < somme) {
			throw new Exceptionvirement();
		} else {
			c2.solde = c2.solde + somme;
			c1.solde = c1.solde - somme;
		}
		// } catch (Exceptionvirement e) {
		// }
		// ;
	}

	public void Crediter(int somme) {
		this.solde = this.solde + somme;
	}

	public void Debiter(int somme) {
		this.solde = this.solde - somme;
	}

}
