package javaProjetEcllipsegit;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ClasseTest {

	@Test
	public void TestConsultation() {
		compteBancair c = new compteBancair(3000);
		assertEquals(3000, c.solde);
	}

	@Test
	public void testOperationV() throws Exceptionvirement {
		compteBancair c1 = new compteBancair(4000);
		compteBancair c2 = new compteBancair(1000);

		c1.virement(c1, c2, 1000);
		assertEquals(3000, c1.solde);
		assertEquals(2000, c2.solde);
	}

	@Test
	public void testCrediter() {
		compteBancair c = new compteBancair(1000);
		c.Crediter(500);
		assertEquals(1500, c.solde);
	}

	@Test
	public void testdeiter() {
		compteBancair c = new compteBancair(2000);
		c.Debiter(2000);
		assertEquals(0, c.solde);
	}

	@Test(expected = Exceptionvirement.class)
	public void testOperationExc() throws Exceptionvirement {
		compteBancair c = new compteBancair(3000);
		compteBancair c1 = new compteBancair(2000);
		c.virement(c, c1, 4000);

	}

}
